/*******************************************************************************
*                               Werise - 2020
*-------------------------------------------------------------------------------
*
*	Class to create a record of campaign member to lead and contacts that has
*   campaign name on certain fields.
*
*   Called by: LeadTrigger, ContactTrigger
*   Test Classes: HistoricoCampanhasTest
*
* NAME: HistoricoCampanhasHandle
* AUTHOR: João Vitor Ramos                                 DATE:22/12/2020
*******************************************************************************/
public class HistoricoCampanhasHandle {
    public static void addCampaingMemberUtmLead(List<Lead> leads){
        
        //Lists, sets and Maps to handle the logic
        set<String> CampaignNameSet = new set<String>();
        list<Campaign> CampaignIdList = new list<Campaign>();
        map<String,Id> CampaignNameMap = new map<String, Id>();
        list<Lead> CampaignLeadList = new list<Lead>();
        map<String,String> CampaignMembersMap = new map<String, String>();

        //Making the map of name and Id of each campaingn related to the leads
        for(Lead ld : leads){
            CampaignNameSet.add('%'+ld.Campaign_Name__c+'%');
        }
        CampaignIdList = [select id, name from campaign where name like :CampaignNameSet];
        for(Campaign cp : CampaignIdList){
            CampaignNameMap.put(cp.name, cp.id);
        }

        //Validating if the Lead if not a member of the new campaign
        CampaignLeadList = [select id, Campaign_Name__c, (select CampaignId, LeadId from CampaignMembers) from lead where id in :leads];
        for(Lead ld : CampaignLeadList){
            if(ld.CampaignMembers.size() > 0){
                for(CampaignMember cm : ld.CampaignMembers){
                    Integer i = 0;
                    if(CampaignNameMap.get(ld.Campaign_Name__c) == cm.CampaignId){
                        break;
                    }
                    if(CampaignNameMap.get(ld.Campaign_Name__c) != cm.CampaignId){
                        i++;
                        if(i == ld.CampaignMembers.size()){
                            CampaignMembersMap.put(ld.id, CampaignNameMap.get(ld.Campaign_Name__c));
                        }
                    }
                }
            }
            if(ld.CampaignMembers.size() < 1){
                CampaignMembersMap.put(ld.id, CampaignNameMap.get(ld.Campaign_Name__c));
            }
        }

        if(!CampaignMembersMap.isEmpty()){
            criarMembrosLead(CampaignMembersMap);
        }
    }

    public static void addCampaingMemberGoogleLead(list<Lead> leads){
        //Id CampaignId = (ID)Database.Query('select id from Campaign where name like \'%'+ld.Campaign_Name__c+'%\'')[0].id;
        
        //Lists, sets and Maps to handle the logic
        set<String> CampaignNameSet = new set<String>();
        list<Campaign> CampaignIdList = new list<Campaign>();
        map<String,Id> CampaignNameMap = new map<String, Id>();
        list<Lead> CampaignLeadList = new list<Lead>();
        map<String,String> CampaignMembersMap = new map<String, String>();

        //Making the map of name and Id of each campaingn related to the leads
        for(Lead ld : leads){
            CampaignNameSet.add('%'+ld.pi__utm_campaign__c+'%');
        }
        CampaignIdList = [select id, name from campaign where name like :CampaignNameSet];
        for(Campaign cp : CampaignIdList){
            CampaignNameMap.put(cp.name, cp.id);
        }

        //Validating if the Lead if not a member of the new campaign
        CampaignLeadList = [select id, pi__utm_campaign__c, (select CampaignId, LeadId from CampaignMembers) from lead where id in :leads];
        for(Lead ld : CampaignLeadList){
            if(ld.CampaignMembers.size() > 0){
                for(CampaignMember cm : ld.CampaignMembers){
                    Integer i = 0;
                    if(CampaignNameMap.get(ld.pi__utm_campaign__c) == cm.CampaignId){
                        break;
                    }
                    if(CampaignNameMap.get(ld.pi__utm_campaign__c) != cm.CampaignId){
                        i++;
                        if(i == ld.CampaignMembers.size()){
                            CampaignMembersMap.put(ld.id, CampaignNameMap.get(ld.pi__utm_campaign__c));
                        }
                    }
                }
            }
            if(ld.CampaignMembers.size() < 1){
                CampaignMembersMap.put(ld.id, CampaignNameMap.get(ld.pi__utm_campaign__c));
            }
        }

        if(!CampaignMembersMap.isEmpty()){
            criarMembrosLead(CampaignMembersMap);
        }
    }

    //----------------- CONTACT METHODS --------------------------------//
    public static void addCampaingMemberUtmContact(List<Contact> contacts){
        
        //Lists, sets and Maps to handle the logic
        set<String> CampaignNameSet = new set<String>();
        list<Campaign> CampaignIdList = new list<Campaign>();
        map<String,Id> CampaignNameMap = new map<String, Id>();
        list<Contact> CampaignContactList = new list<Contact>();
        map<String,String> CampaignMembersMap = new map<String, String>();

        //Making the map of name and Id of each campaingn related to the leads
        for(Contact con : contacts){
            CampaignNameSet.add('%'+con.Campaign_Name__c+'%');
        }
        CampaignIdList = [select id, name from campaign where name like :CampaignNameSet];
        for(Campaign cp : CampaignIdList){
            CampaignNameMap.put(cp.name, cp.id);
        }

        //Validating if the Lead if not a member of the new campaign
        CampaignContactList = [select id, Campaign_Name__c, (select CampaignId, ContactId from CampaignMembers) from Contact where id in :contacts];
        for(Contact con : CampaignContactList){
            if(con.CampaignMembers.size() > 0){
                for(CampaignMember cm : con.CampaignMembers){
                    Integer i = 0;
                    if(CampaignNameMap.get(con.Campaign_Name__c) == cm.CampaignId){
                        break;
                    }
                    if(CampaignNameMap.get(con.Campaign_Name__c) != cm.CampaignId){
                        i++;
                        if(i == con.CampaignMembers.size()){
                            CampaignMembersMap.put(con.id, CampaignNameMap.get(con.Campaign_Name__c));
                        }
                    }
                }
            }
            if(con.CampaignMembers.size() < 1){
                CampaignMembersMap.put(con.id, CampaignNameMap.get(con.Campaign_Name__c));
            }
        }

        if(!CampaignMembersMap.isEmpty()){
            criarMembrosContact(CampaignMembersMap);
        }
    }

    public static void addCampaingMemberGoogleContact(list<Contact> contacts){
        //Id CampaignId = (ID)Database.Query('select id from Campaign where name like \'%'+ld.Campaign_Name__c+'%\'')[0].id;
        
        //Lists, sets and Maps to handle the logic
        set<String> CampaignNameSet = new set<String>();
        list<Campaign> CampaignIdList = new list<Campaign>();
        map<String,Id> CampaignNameMap = new map<String, Id>();
        list<Contact> CampaignContactList = new list<Contact>();
        map<String,String> CampaignMembersMap = new map<String, String>();

        //Making the map of name and Id of each campaingn related to the leads
        for(Contact con : contacts){
            CampaignNameSet.add('%'+con.pi__utm_campaign__c+'%');
        }
        CampaignIdList = [select id, name from campaign where name like :CampaignNameSet];
        for(Campaign cp : CampaignIdList){
            CampaignNameMap.put(cp.name, cp.id);
        }

        //Validating if the Lead if not a member of the new campaign
        CampaignContactList = [select id, pi__utm_campaign__c, (select CampaignId, ContactId from CampaignMembers) from Contact where id in :contacts];
        for(Contact con : CampaignContactList){
            if(con.CampaignMembers.size() > 0){
                for(CampaignMember cm : con.CampaignMembers){
                    Integer i = 0;
                    if(CampaignNameMap.get(con.pi__utm_campaign__c) == cm.CampaignId){
                        break;
                    }
                    if(CampaignNameMap.get(con.pi__utm_campaign__c) != cm.CampaignId){
                        i++;
                        if(i == con.CampaignMembers.size()){
                            CampaignMembersMap.put(con.id, CampaignNameMap.get(con.pi__utm_campaign__c));
                        }
                    }
                }
            }
            if(con.CampaignMembers.size() < 1){
                CampaignMembersMap.put(con.id, CampaignNameMap.get(con.pi__utm_campaign__c));
            }
        }

        if(!CampaignMembersMap.isEmpty()){
            criarMembrosContact(CampaignMembersMap);
        }
    }

    //method to create the CampaignMember record to the leads
    public static void criarMembrosLead(map<String,String> membros){
        list<CampaignMember> membrosUpd = new list<CampaignMember>();
        for(String ld : membros.keyset()){
            CampaignMember cm = new CampaignMember();
            cm.CampaignId = membros.get(ld);
            cm.LeadId = ld;
            cm.Status = 'Enviado';

            membrosUpd.add(cm);
        }
        if(!membrosUpd.isEmpty()){
            try {
                insert membrosUpd;
            } catch(DmlException e) {
                System.debug('The following exception has occurred: ' + e.getMessage());
            }   
        }       
    }
    //method to create the CampaignMember record to the Contacts
    public static void criarMembrosContact(map<String,String> membros){
        list<CampaignMember> membrosUpd = new list<CampaignMember>();
        for(String con : membros.keyset()){
            CampaignMember cm = new CampaignMember();
            cm.CampaignId = membros.get(con);
            cm.ContactId = con;
            cm.Status = 'Enviado';

            membrosUpd.add(cm);
        }
        if(!membrosUpd.isEmpty()){
            try {
                insert membrosUpd;
            } catch(DmlException e) {
                System.debug('The following exception has occurred: ' + e.getMessage());
            }   
        }       
    }
}
