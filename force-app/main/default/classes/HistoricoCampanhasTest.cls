/*******************************************************************************
*                               Werise - 2020
*-------------------------------------------------------------------------------
*
*	Test Class to test the creation record of campaign member to lead and contacts 
*   that has campaign name on certain fields.
*
*   Testing: LeadTrigger, ContactTrigger, HistoricoCampanhasHandle
*
* NAME: HistoricoCampanhasTest
* AUTHOR: João Vitor Ramos                                 DATE:22/12/2020
*******************************************************************************/
@isTest
public class HistoricoCampanhasTest {

    private static list<Campaign> makeCampaigns(){
        List<Campaign> campaignList = new List<Campaign>();
        for(Integer i = 1; i<=2; i++){
            Campaign cp = new Campaign();
            cp.name = '[Campanha][Teste]'+i;
            campaignList.add(cp);
        }
        insert campaignList;
        return campaignList;
    }

    private static Account makeAccount(){
        Account acc = new Account();
        acc.name = 'Conta Teste';
        insert acc;
        return acc;
    }
    
    private static list<Lead> makeLeads(){
        List<Lead> leadList = new List<Lead>();
        List<Campaign> campaignList = makeCampaigns();

        for(Integer i = 0; i<5; i++){
            Lead ld = new Lead();
            ld.RecordTypeId = [select id from recordtype where sobjecttype = 'lead' and name = 'Inbound'][0].Id;
            ld.LastName = 'teste' + (i+1);
            ld.Company = 'Werise';
            ld.Status = getStatus();

            switch on i {
                when 0 {
                    ld.Campaign_Name__c = campaignList[0].name;
                }
                when 1 {
                    ld.pi__utm_campaign__c = campaignList[1].name;
                }
                when 2 {
                    ld.Campaign_Name__c = campaignList[0].name;
                    ld.pi__utm_campaign__c = campaignList[0].name;
                }
                when 3 {
                    ld.Campaign_Name__c = campaignList[0].name;
                    ld.pi__utm_campaign__c = campaignList[1].name;
                }
                when else {
                    ld.Campaign_Name__c = '...';
                    ld.pi__utm_campaign__c = '...';
                }
            }
            leadList.add(ld);
        }
        return leadList;
    }

    private static list<Contact> makeContacts(){
        List<Contact> contactList = new List<Contact>();
        List<Campaign> campaignList = makeCampaigns();
        Account acc = makeAccount();

        for(Integer i = 0; i<5; i++){
            Contact con = new Contact();
            con.LastName = 'teste' + (i+1);
            con.Accountid = acc.Id;

            switch on i {
                when 0 {
                    con.Campaign_Name__c = campaignList[0].name;
                }
                when 1 {
                    con.pi__utm_campaign__c = campaignList[1].name;
                }
                when 2 {
                    con.Campaign_Name__c = campaignList[0].name;
                    con.pi__utm_campaign__c = campaignList[0].name;
                }
                when 3 {
                    con.Campaign_Name__c = campaignList[0].name;
                    con.pi__utm_campaign__c = campaignList[1].name;
                }
                when else {
                    con.Campaign_Name__c = '...';
                    con.pi__utm_campaign__c = '...';
                }
            }
            contactList.add(con);
        }
        return contactList;
    }

    private static String getStatus(){
        List<String> pickListValuesList= new List<String>();
        Schema.DescribeFieldResult fieldResult = Lead.Status.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getLabel());
        }     
        return pickListValuesList[0];
    }

    @isTest
    private static void leadInsert(){
        List<Lead> leadList = makeLeads();
        list<Id> leadIds = new list<Id>();
        list<CampaignMember> cmList = new list<CampaignMember>();
        list<Lead> leadCmList = new list<Lead>();

        Test.startTest();
        insert leadList;
        Test.stopTest();

        for(Lead ld : leadList){
            leadIds.add(ld.id);
        }

        cmList = [select name, leadid from CampaignMember where leadid in :leadIds];
        leadCmList = [select id from lead where id in (select leadid from CampaignMember where campaign.name = '[campanha][teste]1')];

        System.assertEquals(5, cmList.size());
        System.assertEquals(3, leadCmList.size());
    }

    @isTest
    private static void leadUpdate(){
        List<Lead> leadList = makeLeads();
        List<Lead> leadListUpd = new List<Lead>();
        list<CampaignMember> cmList = new list<CampaignMember>();
        list<Lead> leadCmList = new list<Lead>();
        list<Lead> leadCmList2 = new list<Lead>();

        list<Id> leadIds = new list<Id>();
        list<Campaign> campaignList = [select name from campaign order by name asc];
        
        insert leadList;

        for(Lead ld : leadList){
            Integer i = 0;
            switch on i {
                when 0 {
                    ld.Campaign_Name__c = campaignList[1].name;
                }
                when 1 {
                    ld.pi__utm_campaign__c = campaignList[0].name;
                }
                when 2 {
                    ld.Campaign_Name__c = campaignList[1].name;
                    ld.pi__utm_campaign__c = campaignList[1].name;
                }
                when 3 {
                    ld.Campaign_Name__c = campaignList[1].name;
                    ld.pi__utm_campaign__c = campaignList[0].name;
                }
                when else {
                    ld.Campaign_Name__c = ' ';
                    ld.pi__utm_campaign__c = '...';
                }
            }
            i++;
            leadListUpd.add(ld);
        }

        Test.startTest();
        update leadListUpd;
        Test.stopTest();

        for(Lead ld : leadListUpd){
            leadIds.add(ld.id);
        }

        cmList = [select name, leadid from CampaignMember where leadid in :leadIds];
        leadCmList = [select id from lead where id in (select leadid from CampaignMember where campaign.name = '[campanha][teste]1')];
        leadCmList2 = [select id from lead where id in (select leadid from CampaignMember where campaign.name != '[campanha][teste]1')];

        
        system.debug(cmList);
        system.debug(leadCmList);
        system.debug(leadCmList2);

        System.assertEquals(8, cmList.size());
        System.assertEquals(3, leadCmList.size());
        System.assertEquals(5, leadCmList2.size());
    }

    @isTest
    private static void contactInsert(){
        list<Contact> contactList = makeContacts();
        list<Id> contactIds = new list<Id>();
        list<CampaignMember> cmList = new list<CampaignMember>();
        list<Contact> conCmList = new list<Contact>();

        Test.startTest();
        insert contactList;
        Test.stopTest();

        for(Contact con : contactList){
            contactIds.add(con.id);
        }

        cmList = [select name, contactid from CampaignMember where contactId in :contactIds];
        conCmList = [select id from contact where id in (select contactid from CampaignMember where campaign.name = '[campanha][teste]1')];

        System.assertEquals(5, cmList.size());
        System.assertEquals(3, conCmList.size());
    }

    @isTest
    private static void contactUpdate(){
        list<Contact> contactList = makeContacts();
        List<Contact> contactListUpd = new List<Contact>();
        list<CampaignMember> cmList = new list<CampaignMember>();
        list<Contact> conCmList = new list<Contact>();
        list<Contact> conCmList2 = new list<Contact>();
        list<Id> contactIds = new list<Id>();
        list<Campaign> campaignList = [select name from campaign order by name asc];
        
        insert contactList;

        for(Contact con : contactList){
            Integer i = 0;
            switch on i {
                when 0 {
                    con.Campaign_Name__c = campaignList[1].name;
                }
                when 1 {
                    con.pi__utm_campaign__c = campaignList[0].name;
                }
                when 2 {
                    con.Campaign_Name__c = campaignList[1].name;
                    con.pi__utm_campaign__c = campaignList[1].name;
                }
                when 3 {
                    con.Campaign_Name__c = campaignList[1].name;
                    con.pi__utm_campaign__c = campaignList[0].name;
                }
                when else {
                    con.Campaign_Name__c = ' ';
                    con.pi__utm_campaign__c = '...';
                }
            }
            i++;
            contactListUpd.add(con);
        }

        Test.startTest();
        update contactListUpd;
        Test.stopTest();

        for(Contact con : contactListUpd){
            contactIds.add(con.id);
        }

        cmList = [select name, contactId from CampaignMember where contactId in :contactIds];
        conCmList = [select id from contact where id in (select contactid from CampaignMember where campaign.name = '[campanha][teste]1')];
        conCmList2 = [select id from contact where id in (select contactid from CampaignMember where campaign.name != '[campanha][teste]1')];


        System.assertEquals(8, cmList.size());
        System.assertEquals(3, conCmList.size());
        System.assertEquals(5, conCmList2.size());
    }

}
