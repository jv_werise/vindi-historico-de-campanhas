/*******************************************************************************
*                               Werise - 2020
*-------------------------------------------------------------------------------
*
*	Trigger on Contact SObject for (Insert or Update) && (After or Before)
*
*	For Insert:
*   HistoricoCampanhasHandle.addCampaingMemberUtmContact();
*   HistoricoCampanhasHandle.addCampaingMemberGoogleContact();
*
*   For Updtade:
*   HistoricoCampanhasHandle.addCampaingMemberUtmContact();
*   HistoricoCampanhasHandle.addCampaingMemberGoogleContact();
*   ReturnOfLostContact.checkContactRole();
*
*   Test Classes: HistoricoCampanhasTest
*
* NAME: ContactTrigger
* AUTHOR: Jefferson F. Presotto                                DATE: 28/02/2020
* UPDATED BY: João Vitor Ramos                                 DATE:22/12/2020
*******************************************************************************/

trigger ContactTrigger on Contact (before update, after update, before insert, after insert) {

    List<Contact> contactList = new List<Contact>();

    System.debug('%%%%%%%%%%%%%%%% CONTACT TRIGGER IS RUNNING %%%%%%%%%%%%%%%%');

    //If trigger is update
    if(Trigger.isUpdate){
        List<Id> contactToUpdateFuture = new List<Id>();

        //before update
        if(Trigger.isBefore){
            System.debug('%%%%%%%%%%%%%%%% CONTACT TRIGGER IS RUNNING BEFORE UPDATE %%%%%%%%%%%%%%%%');
            for(Contact con : Trigger.new){
                if(con.Retornou_do_Lost__c == true && trigger.oldMap.get(con.Id).Retornou_do_Lost__c != true){
                    contactList.add(con);
                }
                else if(con.Criacao_de_Oportunidade__c == true && trigger.oldMap.get(con.Id).Criacao_de_Oportunidade__c != true){
                    //create the opportunity with process builder and put the contact to update in future
                    contactToUpdateFuture.add(con.Id);
                }
            }
            //call the opportunity lost process
            if(!contactList.isEmpty()) ReturnOfLostContact.checkContactRole(contactList);

            if(!contactToUpdateFuture.isEmpty()){
                // Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
                DateTime lToday = system.now();
                Integer minutes = lToday.minute() + 5;
                Integer hour = lToday.hour();
                if(minutes >= 60){
                    minutes = 1;    
                    hour += 1;
                }
                //update the contact in future for sync with pardot correctly
                String TNAME = 'update contact ' + System.now() + contactToUpdateFuture;
                String cron = '0 ' + minutes + ' ' + hour + ' ' + lToday.day() + ' ' + lToday.month() + ' ? ' + lToday.year();
                UpdateContactFuture sch = new UpdateContactFuture(contactToUpdateFuture);
                System.schedule(TNAME + cron, cron, sch);
            }
        }

        //after update
        if(Trigger.isAfter){
            List<Contact> listContactToCommercial = new List<Contact>();
            for(Contact con : Trigger.new){
                if(con.Atribuir_ao_Comercial__c == true && trigger.oldMap.get(con.Id).Atribuir_ao_Comercial__c != con.Atribuir_ao_Comercial__c){
                    if(con.Proprietariado_do_Funil__c == null || con.Proprietariado_do_Funil__c == ''){ 
                        con.Proprietariado_do_Funil__c.addError('É necessário atribuir um Proprietário do Funil para que possa ser atribuído ao comercial');
                        continue;
                    } else {
                        listContactToCommercial.add(con);
                    }
                }
            }
            //call the assign lead to commercial process
            if(!listContactToCommercial.isEmpty()) AssignLeadtoCommercial.createOpportunity(listContactToCommercial);


            //Campaign Member Automation
            system.debug('---------------CampaignMember automation insert-----------------');
            List<Contact> campaignList = new List<Contact>();
            List<Contact> googleList = new List<Contact>();

            for(Contact con : Trigger.new){
                //Lists to handle the logic
                if((con.Campaign_Name__c != null && con.Campaign_Name__c != '' && con.Campaign_Name__c != ' ' && con.Campaign_Name__c != '...' && con.Campaign_Name__c != Trigger.oldMap.get(con.id).Campaign_Name__c) ||
                (con.pi__utm_campaign__c != null && con.pi__utm_campaign__c != '' && con.pi__utm_campaign__c != ' ' && con.pi__utm_campaign__c != '...' && con.pi__utm_campaign__c != Trigger.oldMap.get(con.id).pi__utm_campaign__c)){
                    if(con.Campaign_Name__c == con.pi__utm_campaign__c){
                        campaignList.add(con);
                    }
                    if(con.Campaign_Name__c != null && con.Campaign_Name__c != '' && con.Campaign_Name__c != ' ' && con.Campaign_Name__c != '...' && con.Campaign_Name__c != con.pi__utm_campaign__c && con.Campaign_Name__c != Trigger.oldMap.get(con.id).Campaign_Name__c){
                        campaignList.add(con);
                    }
                    if(con.pi__utm_campaign__c != null && con.pi__utm_campaign__c != '' && con.pi__utm_campaign__c != ' ' && con.pi__utm_campaign__c != '...' && con.pi__utm_campaign__c != con.Campaign_Name__c && con.pi__utm_campaign__c != Trigger.oldMap.get(con.id).pi__utm_campaign__c){
                        googleList.add(con);
                    }
                }
            }

            if(!campaignList.isEmpty()){
                HistoricoCampanhasHandle.addCampaingMemberUtmContact(campaignList);
            }
            if(!googleList.isEmpty()){
                HistoricoCampanhasHandle.addCampaingMemberGoogleContact(googleList);
            }
        }
    }

    //If trigger is insert
    if(Trigger.isInsert){
    
        //before insert
        if(Trigger.isBefore){
            //split the first name into last name
            for(Contact con : Trigger.new){
                if(con.LastName == null || con.LastName == ''){
                    String firstName, lastName;
                    integer index = 0;
                    for (String str : con.FirstName.split(' ')){
                        if(index == 0){
                            firstName = str;
                            index++;
                            continue;
                        }
                        else if(index == 1){ 
                            lastName = str;
                            index++;
                        }
                        else { 
                            lastName += ' ' + str;
                        }
                    }
                    con.FirstName = firstName;
                    if(lastName == '' || lastName == null) lastName = '.';
                    con.LastName = lastName;
                }
            }
        }

        if(Trigger.isAfter){
            //Campaign Member Automation
            system.debug('---------------CampaignMember automation insert-----------------');
            List<Contact> campaignList = new List<Contact>();
            List<Contact> googleList = new List<Contact>();

            for(Contact con : Trigger.new){
                //Lists to handle the logic
                if((con.Campaign_Name__c != null && con.Campaign_Name__c != '' && con.Campaign_Name__c != ' ' && con.Campaign_Name__c != '...') ||
                (con.pi__utm_campaign__c != null && con.pi__utm_campaign__c != '' && con.pi__utm_campaign__c != ' ' && con.pi__utm_campaign__c != '...')){
                    if(con.Campaign_Name__c == con.pi__utm_campaign__c){
                        campaignList.add(con);
                    }
                    if(con.Campaign_Name__c != null && con.Campaign_Name__c != '' && con.Campaign_Name__c != ' ' && con.Campaign_Name__c != '...' && con.Campaign_Name__c != con.pi__utm_campaign__c){
                        campaignList.add(con);
                    }
                    if(con.pi__utm_campaign__c != null && con.pi__utm_campaign__c != '' && con.pi__utm_campaign__c != ' ' && con.pi__utm_campaign__c != '...' && con.pi__utm_campaign__c != con.Campaign_Name__c){
                        googleList.add(con);
                    }
                }
            }

            if(!campaignList.isEmpty()){
                HistoricoCampanhasHandle.addCampaingMemberUtmContact(campaignList);
            }
            if(!googleList.isEmpty()){
                HistoricoCampanhasHandle.addCampaingMemberGoogleContact(googleList);
            }
        }
    }
}