/*******************************************************************************
*                               Werise - 2020
*-------------------------------------------------------------------------------
*
*	Trigger on Lead SObject for (Insert or Update) && (After or Before)
*
* 	For Insert:
*   LeadQueueDistribution.distributeLead();
*   HistoricoCampanhasHandle.addCampaingMemberUtmLead();
*   HistoricoCampanhasHandle.addCampaingMemberGoogleLead();
*
*	For Update:
*   LeadQueueDistribution.distributeLead();
*   HistoricoCampanhasHandle.addCampaingMemberUtmLead();
*   HistoricoCampanhasHandle.addCampaingMemberGoogleLead();
*
*   Test Class: HistoricoCampanhasTest
*
* NAME: LeadTrigger
* AUTHOR: Jefferson F. Presotto                                DATE: 01/10/2019
* UPDATED BY: João Vitor Ramos                                 DATE:22/12/2020
*******************************************************************************/

trigger LeadTrigger on Lead (before insert, before update, after insert, after update) {

    System.debug('%%%%%%%%%%%%%%%% LEAD TRIGGER IS RUNNING %%%%%%%%%%%%%%%%');
    List<Lead> leadsUpdated = new List<Lead>();
    Map<String, String> map_UserName_UserId = new Map<String, String>();

    for(User user : [SELECT Id, Name FROM User WHERE isActive = true]){
        map_UserName_UserId.put(user.Name, user.Id);
    }

    //If trigger is insert
    if(Trigger.isInsert){

        //before insert
        if(Trigger.isBefore){
            System.debug('%%%%%%%%%%%%%%%% LEAD TRIGGER IS RUNNING BEFORE INSERT %%%%%%%%%%%%%%%%');

            //split the first name into last name
            for(Lead ld : Trigger.new){
                String firstName, lastName;

                if(ld.LastName == null || ld.LastName == '' || ld.LastName.contains('Unknown')){
                    String fullName = ld.FirstName;
                    integer index = 0;
                    for (String str : ld.FirstName.split(' ')){
                        if(index == 0){
                            firstName = str;
                            index++;
                            continue;
                        }
                        else if(index == 1){ 
                            lastName = str;
                            index++;
                        }
                        else{ 
                            lastName += ' ' + str;
                        }
                    }
                    ld.FirstName = firstName;
                    if(lastName == '' || lastName == null) lastName = '.';
                    ld.LastName = lastName;
                }
                //if the 'Proprietariado_do_Funil__c' is not blank change the owner
                if(ld.Atribuir_ao_Comercial__c == true && ld.IsConverted == false){
                    if(ld.Proprietariado_do_Funil__c == null || ld.Proprietariado_do_Funil__c == ''){
                        ld.Proprietariado_do_Funil__c.addError('É necessário atribuir um Proprietário do Funil para que possa ser atribuído ao comercial');
                        continue;
                    }
                    ld.OwnerId = map_UserName_UserId.get(ld.Proprietariado_do_Funil__c);
                }
            }
            if(system.isFuture()) return;
            LeadQueueDistribution.distributeLead(Trigger.new);
        }

        //after insert
        if(Trigger.isAfter){
            System.debug('%%%%%%%%%%%%%%%% LEAD TRIGGER IS RUNNING AFTER INSERT %%%%%%%%%%%%%%%%');

            //verify if the lead is to the comercial
            List<Lead> leadList = new List<Lead>();
            for(Lead ld : Trigger.new){
                if(ld.Atribuir_ao_Comercial__c == true && ld.IsConverted == false){
                    if(ld.Proprietariado_do_Funil__c == null || ld.Proprietariado_do_Funil__c == ''){
                        ld.Proprietariado_do_Funil__c.addError('É necessário atribuir um Proprietário do Funil para que possa ser atribuído ao comercial');
                        continue;   
                    }
                    leadList.add(ld);
                }
            }
            if(!leadList.isEmpty()) AssignLeadtoCommercial.verifyLead(leadList);


            //Campaign Member Automation
            system.debug('---------------CampaignMember automation insert-----------------');
            List<Lead> campaignList = new List<Lead>();
            List<Lead> googleList = new List<Lead>();

            for(Lead ld : Trigger.new){
                //Lists to handle the logic
                if((ld.Campaign_Name__c != null && ld.Campaign_Name__c != '' && ld.Campaign_Name__c != ' ' && ld.Campaign_Name__c != '...') ||
                (ld.pi__utm_campaign__c != null && ld.pi__utm_campaign__c != '' && ld.pi__utm_campaign__c != ' ' && ld.pi__utm_campaign__c != '...')){
                    if(ld.Campaign_Name__c == ld.pi__utm_campaign__c){
                        campaignList.add(ld);
                    }
                    if(ld.Campaign_Name__c != null && ld.Campaign_Name__c != '' && ld.Campaign_Name__c != ' ' && ld.Campaign_Name__c != '...' && ld.Campaign_Name__c != ld.pi__utm_campaign__c){
                        campaignList.add(ld);
                    }
                    if(ld.pi__utm_campaign__c != null && ld.pi__utm_campaign__c != '' && ld.pi__utm_campaign__c != ' ' && ld.pi__utm_campaign__c != '...' && ld.pi__utm_campaign__c != ld.Campaign_Name__c){
                        googleList.add(ld);
                    }
                }
            }

            if(!campaignList.isEmpty()){
                HistoricoCampanhasHandle.addCampaingMemberUtmLead(campaignList);
            }
            if(!googleList.isEmpty()){
                HistoricoCampanhasHandle.addCampaingMemberGoogleLead(googleList);
            }
        }
    }

    //If trigger is update
    else if(Trigger.isUpdate){

        //before update
        if(Trigger.isBefore){
            System.debug('%%%%%%%%%%%%%%%% LEAD TRIGGER IS RUNNING BEFORE UPDATE %%%%%%%%%%%%%%%%');

            for(Lead ld : Trigger.new){
                //if the 'Proprietariado_do_Funil__c' is not blank change the owner
                if(ld.Atribuir_ao_Comercial__c == true && trigger.oldMap.get(ld.Id).Atribuir_ao_Comercial__c == false && ld.IsConverted == false){
                    if(ld.Proprietariado_do_Funil__c == null || ld.Proprietariado_do_Funil__c == ''){
                        ld.Proprietariado_do_Funil__c.addError('É necessário atribuir um Proprietário do Funil para que possa ser atribuído ao comercial');
                        continue;
                    } else {
                        ld.OwnerId = map_UserName_UserId.get(ld.Proprietariado_do_Funil__c);
                    }
                }
            }
            if(system.isFuture()) return;
            LeadQueueDistribution.distributeLead(Trigger.new);
        }

        //after update
        if(Trigger.isAfter){
            System.debug('%%%%%%%%%%%%%%%% LEAD TRIGGER IS RUNNING AFTER UPDATE %%%%%%%%%%%%%%%%');

            List<Lead> leadList = new List<Lead>();
            //verify if the lead is to the comercial
            for(Lead ld : Trigger.new){
                if(ld.Atribuir_ao_Comercial__c == true && trigger.oldMap.get(ld.Id).Atribuir_ao_Comercial__c == false && ld.IsConverted == false){
                    if(ld.Proprietariado_do_Funil__c == null || ld.Proprietariado_do_Funil__c == ''){
                        ld.Proprietariado_do_Funil__c.addError('É necessário atribuir um Proprietário do Funil para que possa ser atribuído ao comercial');
                        continue;
                    }
                    leadList.add(ld);
                }
            }   
            //call the assign lead to commercial process
            if(!leadList.isEmpty()) AssignLeadtoCommercial.verifyLead(leadList);


            //Campaign Member Automation
            system.debug('---------------CampaignMember automation update-----------------');
            List<Lead> campaignList = new List<Lead>();
            List<Lead> googleList = new List<Lead>();

            for(Lead ld : Trigger.new){
                //Lists to handle the logic
                if((ld.Campaign_Name__c != null && ld.Campaign_Name__c != '' && ld.Campaign_Name__c != ' ' && ld.Campaign_Name__c != '...' && ld.Campaign_Name__c != Trigger.oldMap.get(ld.id).Campaign_Name__c) ||
                (ld.pi__utm_campaign__c != null && ld.pi__utm_campaign__c != '' && ld.pi__utm_campaign__c != ' ' && ld.pi__utm_campaign__c != '...' && ld.pi__utm_campaign__c != Trigger.oldMap.get(ld.id).pi__utm_campaign__c)){
                    if(ld.Campaign_Name__c == ld.pi__utm_campaign__c){
                        campaignList.add(ld);
                    }
                    if(ld.Campaign_Name__c != null && ld.Campaign_Name__c != '' && ld.Campaign_Name__c != ' ' && ld.Campaign_Name__c != '...' && ld.Campaign_Name__c != ld.pi__utm_campaign__c && ld.Campaign_Name__c != Trigger.oldMap.get(ld.id).Campaign_Name__c){
                        campaignList.add(ld);
                    }
                    if(ld.pi__utm_campaign__c != null && ld.pi__utm_campaign__c != '' && ld.pi__utm_campaign__c != ' ' && ld.pi__utm_campaign__c != '...' && ld.pi__utm_campaign__c != ld.Campaign_Name__c && ld.pi__utm_campaign__c != Trigger.oldMap.get(ld.id).pi__utm_campaign__c){
                        googleList.add(ld);
                    }
                }
            }

            if(!campaignList.isEmpty()){
                HistoricoCampanhasHandle.addCampaingMemberUtmLead(campaignList);
            }
            if(!googleList.isEmpty()){
                HistoricoCampanhasHandle.addCampaingMemberGoogleLead(googleList);
            }
        }
    }
}